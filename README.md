# Project Title: EmPOA

### Description:

An app that allows ordinary users in the urban capital to invest in private greentech projects in satellite cities and less urban areas, thus:

- stimulating decentralization
- building wealth through the investments and jobs created
- improving the standard of life by mitigating damage to the climate

The MVP will include the following features:

- Dashboard summarizing: portfolio, profits
- Analytics screen
- Categories for investment eg: E-mobility, Waste Management, Renewable energy
- Marketplace with available projects to invest in

### Installation & Running:
